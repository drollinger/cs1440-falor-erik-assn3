import Deck
import Menu


class UserInterface:
    def run(self):
        """Present the main menu to the user and repeatedly prompt for a valid command"""
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")
        
        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                self.__createDeck()
            elif command == "X":
                keepGoing = False

    def __createDeck(self):
        """Command to create a new Deck"""
        """Get the user to specify the card size, max number, and number of cards"""
        keepGoing = True
        dontStop = True
        while keepGoing:
            cardSpecs = input("Enter card size(3-15), max number(2(Size)^2-4(Size)^2), and number of cards(3-10000): \n").replace(" ", "").split(",")
            if len(cardSpecs) is 3:
                if cardSpecs[0].isdigit() and cardSpecs[1].isdigit() and cardSpecs[2].isdigit():
                    for number in range(0, len(cardSpecs)):
                        cardSpecs[number] = int(cardSpecs[number])
                    if 3 <= cardSpecs[0] <= 15 and\
                    2*pow(cardSpecs[0], 2) <= cardSpecs[1] <= 4*pow(cardSpecs[0], 2) and\
                    3 <= cardSpecs[2] <= 10000:
                        keepGoing = False
            elif len(cardSpecs) is 1:
                if cardSpecs[0] == 'X':
                    keepGoing = False
                    dontStop = False

        if dontStop:
            """Make the Deck"""
            self.__theDeck = Deck.Deck(cardSpecs[0], cardSpecs[2], cardSpecs[1])

            """Print out the Deck Menu"""
            self.__deckMenu()

    def __deckMenu(self):
        """Present the deck menu to user until a valid selection is chosen"""
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen")
        menu.addOption("D", "Display the whole deck to the screen")
        menu.addOption("S", "Save the whole deck to a file")

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__theDeck.print()
            elif command == "S":
                self.__saveDeck()
            elif command == "X":
                keepGoing = False

    def __printCard(self):
        """Command to print a single card"""
        cardToPrint = self.__getNumberInput("Id of card to print: \n", 1, self.__theDeck.getCardCount())
        if cardToPrint > 0:
            print()
            self.__theDeck.print(idx=cardToPrint)

    def __saveDeck(self):
        """Command to save a deck to a file"""
        fileName = self.__getStringInput("Enter output file name: \n")
        if fileName != "":
            outputStream = open(fileName, 'w+')
            self.__theDeck.print(outputStream)
            outputStream.close()
            print("Done!")

    def __getNumberInput(self, message, start, end):
        while True:
            number = input(message)
            if number.strip().isdigit():
                number = int(number)
                if start <= number <= end:
                    return number

    def __getStringInput(self, message):
        return input(message)
