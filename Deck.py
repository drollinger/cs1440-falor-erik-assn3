import sys
import Card
import NumberSet


class Deck:
    def __init__(self, cardSize, cardCount, numberMax):
        """Deck constructor"""
        self.__cardSize = cardSize
        self.__cardCount = cardCount
        self.__numberMax = numberMax
        self.__cards = []
        for newCard in range(0, cardCount):
            newNumberSet = NumberSet.NumberSet(self.__numberMax)
            newNumberSet.randomize()
            self.__cards.append(Card.Card(newCard, self.__cardSize, newNumberSet))

    def getCardCount(self):
        """Return an integer: the number of cards in this deck"""
        return self.__cardCount

    def getCard(self, n):
        """Return card N from the deck"""
        card = None
        n -= 1
        if 0 <= n < self.getCardCount():
            card = self.__cards[n]
        return card

    def print(self, file=sys.stdout, idx=None):
        """void function: Print cards from the Deck

        If an index is given, print only that card.
        Otherwise, print each card in the Deck
        """
        if idx is None:
            for idx in range(1, self.__cardCount + 1):
                c = self.getCard(idx)
                c.print(file)
            print('', file=file)
        else:
            self.getCard(idx).print(file)
