import sys
import math


class Card:
    def __init__(self, idnum, size, numberSet):
        """Card constructor"""
        self.__idnum = idnum
        self.__size = size
        self.numberSet = numberSet

    def getId(self):
        """Return an integer: the ID number of the card"""
        return self.__idnum

    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.__size

    def print(self, file=sys.stdout):
        """void function:
        Prints a card to the screen or to an open file object"""
        """Get the spacing"""
        spacing = int(math.log10(self.numberSet.getSize())) + 3
        if spacing < 5:
            spacing = 5

        """Prints the correct format"""
        self.numberSet.restartNumberSet()
        print(f"\nCard #{self.getId()+1}", file=file)
        for value in range(0, self.getSize()):
            print("+" + "-" * spacing, end='', file=file)
        print("+", file=file)
        for i in range(0, self.getSize()):
            for j in range (0, self.getSize()):
                if i == j == (self.getSize()-1)/2:
                    print("|FREE!", end='', file=file)
                else:
                    print("|" + str(self.numberSet.getNext()).center(spacing), end='', file=file)
            print("|", file=file)
            for value in range(0, self.getSize()):
                print("+" + "-" * spacing, end='', file=file)
            print("+", file=file)
        print(file=file)
