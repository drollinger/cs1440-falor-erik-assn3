import random


class NumberSet:
    def __init__(self, size):
        """NumberSet constructor"""
        self.__size = size
        self.__randomNumberArray = []
        self.__numberSetLocation = 0
        if size > 0:
            for integer in range(1, self.__size + 1):
                self.__randomNumberArray.append(integer)
        else:
            self.__randomNumberArray = [None]

    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        return self.__size

    def get(self, index):
        """Return an integer: get the number from this NumberSet at an index"""
        return self.__randomNumberArray[index]

    def randomize(self):
        """void function: Shuffle this NumberSet"""
        for integer in range(0, self.__size):
            randomLocation = random.randint(1, self.__size - 1)
            self.__randomNumberArray[integer], self.__randomNumberArray[randomLocation] = self.__randomNumberArray[randomLocation], self.__randomNumberArray[integer]

    def getNext(self):
        """Return an integer: when called repeatedly return successive values
        from the NumberSet until the end is reached, at which time 'None' is returned"""
        if self.__numberSetLocation < self.getSize():
            self.__numberSetLocation += 1
            return self.__randomNumberArray[self.__numberSetLocation - 1]
        else:
            return None

    def restartNumberSet(self):
        """Sets the location the object is on in the Number Set to 0"""
        self.__numberSetLocation = 0
